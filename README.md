# Varastonhallinta

Demo application for maintaining packages in a warehouse.


## Technologies

* PostgreSQL
* Quarkus, Hibernate Panache
* React, Tailwind CSS


# Prerequisites

* Java 11
* Docker & Docker Compose
* make


# Development

Start up the database containers with:

    make run-database

This starts up two Postgres containers. The one in port 55432 is used
for the application and another Postgres in port 55433 is used for test
cases. 

Then start backend (to port 8080) with:

    make run-backend

Install frontend dependencies:

    (cd frontend && yarn)

And finally, start up frontend (to port 3000) with:

    (cd frontend && yarn start)

Open browser to http://localhost:8080 and login with credentials:
varastonhallinta/varastonhallinta.

After a successful login, open browser to http://localhost:3000

Both backend and frontend hot reloads changes automatically, so just
edit files and check in browser what has changed.


# Production build

Note: database must be running during build so that test cases work. 

Über jar for JVM:

    make build

The built jar can be run with:

    make run-jar
