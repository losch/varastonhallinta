import {useCallback, useState} from "react";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export interface ApiRequest<T> extends RequestInit {
  url: string;
}

export interface ApiResponse<T> {
  state: API_STATES;
  error: null | any;
  response: T | null;
}

export enum API_STATES {
  INIT,
  LOADING,
  ERROR,
  SUCCESS
}

export class ApiError {
  constructor(public message: string) {}
}

/**
 * A hook for doing the heavy lifting when making API requests
 *
 * @param initialData Initial data to return in a ApiResponse object
 * @return Array of two elements where first element is an API response
 *         object and the second element is a function for making API
 *         requests.
 */
function useApiRequest<T>(initialData: T | null = null): [ApiResponse<T>,
                                                          (apiRequest: ApiRequest<T>) => Promise<T>] {

  const [apiResponse, setApiResponse] = useState({
    state: API_STATES.INIT,
    error: null,
    response: initialData
  });

  const doRequest = useCallback(({url, ...config}: ApiRequest<T>) => {
    setApiResponse(r => ({
      ...r,
      state: API_STATES.LOADING,
      error: null
    }));

    return fetch(url, config)
      .then(response => {
        if (!response.ok) {
          throw response.body ?
            new ApiError(response.status + " " + response.statusText + ": " + JSON.stringify(response.body)) :
            new ApiError(response.status + " " + response.statusText);
        }

        return response.headers.get('Content-Type') === 'application/json' ?
               response.json() :
               response.text();
      })
      .then(result => {
        setApiResponse({
          state: API_STATES.SUCCESS,
          error: null,
          response: result
        });
        return result;
      })
      .catch(e => {
        setApiResponse({
          state: API_STATES.ERROR,
          error: e,
          response: null
        });
        throw e;
      });
  }, []);

  return [apiResponse, doRequest];
}

export default useApiRequest;
