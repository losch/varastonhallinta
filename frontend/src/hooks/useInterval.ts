import React, {useEffect, useRef} from 'react';

/**
 * Use interval hook
 * @param callback Callback to call with given interval
 * @param delay Delay between calling the callback in milliseconds
 *
 * See: https://overreacted.io/making-setinterval-declarative-with-react-hooks/
 */
export default function useInterval(callback: () => void, delay: number) {
  // Store the callback to a ref so if the callback is using
  // some state, then it will get the latest state
  const savedCallback: React.MutableRefObject<(() => void) | undefined> = useRef();

  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  useEffect(() => {
    function tick() {
      if (savedCallback.current) {
        savedCallback.current();
      }
    }

    if (delay !== null) {
      const id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
}
