import React, {InputHTMLAttributes} from 'react';

export interface InputProps extends InputHTMLAttributes<HTMLInputElement> {}

const Input = ({...props}: InputProps) =>
  <input {...props} className="h-full w-full outline-none" />;

export default Input;
