import React from "react";

export interface PanelProps {
  children?: React.ReactNode;
  className?: string;
}

const Panel = ({children, className}: PanelProps) =>
    <div className={"p-3 shadow-md bg-white " + (className || '')}>
      {children}
    </div>;

export default Panel;


export interface PanelHeadingProps {
  children?: React.ReactNode;
}

export const PanelHeading = ({children}: PanelHeadingProps) =>
  <h1 className="pb-1 mb-3 text-xl border-b border-gray-400">{children}</h1>;


export interface PanelBodyProps {
  children?: React.ReactNode;
  className?: string;
}

export const PanelBody = ({children, className}: PanelBodyProps) =>
  <div className={className}>{children}</div>;
