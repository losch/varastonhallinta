import React from 'react';

export interface TableProps {
  children?: React.ReactNode;
  className?: string;
  onClick?: () => void;
}

export const Table = ({children, ...props}: TableProps) =>
  <table {...props}>
    {children}
  </table>;

export const Thead = ({children, ...props}: TableProps) =>
  <thead {...props}>{children}</thead>;

export const Tbody = ({children, ...props}: TableProps) =>
  <tbody {...props}>{children}</tbody>;

export const Tr = ({children, ...props}: TableProps) =>
  <tr {...props}>
    {children}
  </tr>;

export interface CellProps {
  children?: React.ReactNode;
  className?: string;
  colSpan?: number;
}

export const Th = ({children, className, ...props}: CellProps) =>
  <th {...props} className={'p-2 border border-gray-300 ' + (className || '')}>
    {children}
  </th>;

export const Td = ({children, className, ...props}: CellProps) =>
  <td {...props} className={'h-12 px-2 border border-gray-300 ' + (className || '')}>
    {children}
  </td>;
