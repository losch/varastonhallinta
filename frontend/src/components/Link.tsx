import React from 'react';
import {Link as ReactRouterLink} from 'react-router-dom';

export interface LinkProps {
  to: string;
  className?: string;
  children?: React.ReactNode;
}

const Link = ({to, className, children}: LinkProps) => {
  return (
    <ReactRouterLink to={to}
                     className={'text-indigo-500 hover:underline hover:text-indigo-500 ' + (className || '')}>
      {children}
    </ReactRouterLink>
  )
}

export default Link;
