import React, {ButtonHTMLAttributes} from "react";

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {}

function Button({className, disabled, children, ...props}: ButtonProps): React.ReactElement {
  const buttonClassName =
    disabled ?
      "px-2 py-1 border border-indigo-600 bg-gray-500 text-gray-100 rounded shadow transition-all ease-out duration-150 cursor-default select-none" :
      "px-2 py-1 border border-indigo-600 bg-indigo-500 hover:bg-white text-gray-100 hover:text-indigo-600 rounded shadow transition-all ease-out duration-150 select-none";

  return (
    <button {...props}
            disabled={disabled}
            className={buttonClassName + ' ' + className}>
      {children}
    </button>
  );
}

export default Button;
