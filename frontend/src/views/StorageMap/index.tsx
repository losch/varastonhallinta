import React, {useEffect} from 'react';
import {Route, useRouteMatch} from 'react-router-dom';
import Panel, {PanelBody, PanelHeading} from "../../components/Panel";
import useApiRequest from "../../hooks/apiRequest";
import * as Api from "../api";
import {StorageListDto} from "../api";
import useInterval from "../../hooks/useInterval";

const Storage = ({storage}: {storage: StorageListDto}) => {
  return (
    <Panel className="mr-3 mb-3 w-56 h-56">
      <PanelHeading>{storage.name}</PanelHeading>
      <PanelBody className="flex flex-wrap">
        {
          [...Array(storage.capacity)].map((_, index) =>
            <div key={index}
                 className={"w-6 h-6 m-1 " +
                            (index < storage.packageCount ? 'bg-indigo-500 ' : 'bg-gray-400' )
                 } />
          )
        }
      </PanelBody>
    </Panel>
  );
}

const StorageMapView = () => {
  const [storageListResponse, requestStorageList] = useApiRequest<StorageListDto[]>();

  // Fetch storage list
  useEffect(() => {
    requestStorageList(Api.fetchStorageList());
  }, [requestStorageList]);

  // Refresh the view automatically
  useInterval(() => {
    requestStorageList(Api.fetchStorageList());
  }, 1000);

  return (
    <div className="flex flex-wrap">
      {
        storageListResponse.response?.map(storage =>
          <Storage key={storage.id}
                   storage={storage} />
        )
      }
    </div>
  );
}

const StorageMapRoutes = () => {
  const {path} = useRouteMatch();
  return (
    <Route path={`${path}`}>
      <StorageMapView />
    </Route>
  );
};

export default StorageMapRoutes;
