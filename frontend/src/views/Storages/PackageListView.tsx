import React, {useEffect} from 'react';
import {API_STATES, ApiResponse} from "../../hooks/apiRequest";
import {StorageDto} from "../api";
import {Table, Tbody, Td, Th, Thead, Tr} from "../../components/Table";
import Button from "../../components/Button";
import LoadingIndicator from "../../components/LoadingIndicator";

interface PackageListViewProps {
  storageResponse: ApiResponse<StorageDto>;
  movePackageId: number | null;
  setMovePackageId: (packageId: number | null) => void;
  deletePackage: (packageId: number) => void;
}

const PackageListView = ({storageResponse,
                          movePackageId,
                          setMovePackageId,
                          deletePackage}: PackageListViewProps) => {

  // If unmounted, remove move package ID if it was set
  useEffect(() => {
    return () => setMovePackageId(null);
  }, [setMovePackageId]);

  return (
    <div>
      <div>
        {
          movePackageId ?
            <div>
              Valitse hyllylistauksesta hylly, johon paketti siirretään.

              <div className="mt-3">
                <Button onClick={() => setMovePackageId(null)}>
                  Peruuta
                </Button>
              </div>
            </div> :
            <Table className="w-full table-fixed">
              <Thead>
                <Tr>
                  <Th className="w-16">ID</Th>
                  <Th>Nimi</Th>
                  <Th/>
                </Tr>
              </Thead>
              {
                storageResponse.state === API_STATES.LOADING &&
                <Tbody>
                  <Tr>
                    <Td colSpan={3}>
                      <LoadingIndicator />
                    </Td>
                  </Tr>
                </Tbody>
              }

              {
                storageResponse.state === API_STATES.ERROR &&
                <Tbody>
                  <Tr>
                    <Td colSpan={3}>
                      <p>Virhe:</p>
                      {JSON.stringify(storageResponse.error, null, 2)}
                    </Td>
                  </Tr>
                </Tbody>
              }

              {
                storageResponse.state === API_STATES.SUCCESS &&
                <Tbody>
                  {
                    storageResponse.response?.packages?.length === 0 &&
                    <Tr>
                      <Td colSpan={3}>
                        Ei paketteja
                      </Td>
                    </Tr>
                  }

                  {
                    storageResponse.response?.packages.map(pkg =>
                      <Tr key={pkg.id}>
                        <Td className="text-right">#{pkg.id}</Td>
                        <Td>{pkg.name}</Td>
                        <Td>
                          <div className="flex flex-wrap -ml-1">
                            <Button className="m-1" onClick={() => setMovePackageId(pkg.id)}>
                              Siirrä
                            </Button>

                            <Button className="m-1" onClick={() => deletePackage(pkg.id)}>
                              Poista
                            </Button>
                          </div>
                        </Td>
                      </Tr>
                    )
                  }
                </Tbody>
              }
            </Table>
        }
      </div>
    </div>
  );
}

export default PackageListView;
