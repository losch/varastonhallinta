import React from "react";
import {Route, useRouteMatch} from "react-router-dom";
import StorageView from "./StorageView";

const StorageRoutes = () => {
  const {path} = useRouteMatch();
  return (
    <Route path={`${path}/:id?`}>
      <StorageView />
    </Route>
  );
};

export default StorageRoutes;
