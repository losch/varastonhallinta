import {useHistory, useParams} from "react-router-dom";
import useApiRequest, {API_STATES} from "../../hooks/apiRequest";
import React, {useEffect, useState} from "react";
import * as Api from "../api";
import {StorageDto, StorageListDto} from "../api";
import Panel, {PanelBody, PanelHeading} from "../../components/Panel";
import StorageListView from "./StorageListView";
import PackageListView from "./PackageListView";

export interface StorageViewParams {
  id: string;
}

const StorageName = () => {
  const {id} = useParams<StorageViewParams>();
  const [storageName, requestStorageName] = useApiRequest<string>();

  useEffect(() => {
    requestStorageName(Api.fetchStorageName(parseInt(id)))
  }, [id, requestStorageName]);

  return (
    storageName.state === API_STATES.SUCCESS ?
      <span>{storageName.response} (#{id})</span> :
      <span>&nbsp;</span>
  );
};

const StorageView = () => {
  const params = useParams<StorageViewParams>();
  const id = params.id ? parseInt(params.id) : null;
  const history = useHistory();

  const [storageListResponse, requestStorageList] = useApiRequest<StorageListDto[]>();
  const [storageResponse, requestStorage] = useApiRequest<StorageDto>();
  const [addNewPackageResponse, requestAddNewPackage] = useApiRequest<number>();
  const [movePackageId, setMovePackageId] = useState<number | null>(null);
  const [, movePackageRequest] = useApiRequest();
  const [, deletePackageRequest] = useApiRequest();

  const selectStorage = (storageId: number) => {
    if (movePackageId != null) {
      movePackageRequest(Api.movePackage(movePackageId, storageId))
        .then(() => {
          setMovePackageId(null);
          // Reload storage and package lists
          requestStorageList(Api.fetchStorageList());
          if (id) {
            requestStorage(Api.fetchStorage(id));
          }
        });
    }
    else {
      history.push(`/storages/${storageId}`);
    }
  };

  const addNewPackage = (name: string) => {
    return requestAddNewPackage(Api.addPackage(name))
      .then(id => {
        // Reload storage and package lists
        requestStorageList(Api.fetchStorageList());
        requestStorage(Api.fetchStorage(id));

        // Navigate to the storage
        history.push(`/storages/${id}`);
      });
  };

  const deletePackage = (packageId: number) => {
    return deletePackageRequest(Api.deletePackage(packageId))
      .then(() => {
        // Reload storage and package lists
        requestStorageList(Api.fetchStorageList());
        if (id) {
          requestStorage(Api.fetchStorage(id));
        }
      });
  };

  // Fetch storage list
  useEffect(() => {
    requestStorageList(Api.fetchStorageList());
  }, [id, requestStorageList])

  // Fetch package list, if a storage has been selected
  useEffect(() => {
    if (id) {
      requestStorage(Api.fetchStorage(id));
    }
  }, [id, requestStorage]);

  return (
    <div className="flex justify-center flex-wrap-reverse">
      <Panel className="mr-3 mb-3 max-w-2xl w-full">
        <PanelHeading>Hyllyt</PanelHeading>
        <PanelBody>
          <StorageListView selectStorage={selectStorage}
                           selectedStorageId={id}
                           isMovingPackage={!!movePackageId}
                           storageListResponse={storageListResponse}
                           addNewPackage={addNewPackage}
                           addNewPackageResponse={addNewPackageResponse} />
        </PanelBody>
      </Panel>

      {
        id &&
        <Panel className="mr-3 mb-3 max-w-2xl w-full">
          <PanelHeading>
            <StorageName />
          </PanelHeading>
          <PanelBody>
            <PackageListView storageResponse={storageResponse}
                             movePackageId={movePackageId}
                             setMovePackageId={setMovePackageId}
                             deletePackage={deletePackage} />
          </PanelBody>
        </Panel>
      }
    </div>
  );
}

export default StorageView;
