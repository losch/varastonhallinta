import {API_STATES, ApiResponse} from "../../hooks/apiRequest";
import {useState, KeyboardEvent} from "react";
import {StorageListDto} from '../api';
import {Table, Tbody, Td, Th, Thead, Tr} from "../../components/Table";
import Button from "../../components/Button";
import Input from "../../components/Input";
import LoadingIndicator from "../../components/LoadingIndicator";

export interface StorageListViewProps {
  selectStorage: (storageId: number) => void;
  selectedStorageId: number | null;
  isMovingPackage: boolean;
  storageListResponse: ApiResponse<StorageListDto[]>;
  addNewPackage: (name: string) => Promise<any>;
  addNewPackageResponse: ApiResponse<number>;
}

const StorageListView = ({selectStorage,
                          selectedStorageId,
                          isMovingPackage,
                          storageListResponse,
                          addNewPackage,
                          addNewPackageResponse}: StorageListViewProps) => {

  const [newPackageName, setNewPackageName] = useState<string>('');

  const isPackageAddingDisabled = newPackageName.length === 0 ||
                                  addNewPackageResponse.state === API_STATES.LOADING ||
                                  !hasFreeSpaceLeft(storageListResponse.response);

  const addNewPackageAndClearField = () => {
    if (!isPackageAddingDisabled) {
      addNewPackage(newPackageName)
        .then(() => {
          // Reset package name field
          setNewPackageName('');
        });
    }
  };

  const addNewPackageOnEnter = (e: KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter') {
      addNewPackageAndClearField();
    }
  }

  const storageRowClassName = (storage: StorageListDto) => {
    return (
      // Highlight the row if the storage is selected
      storage.id === selectedStorageId ?
        'bg-indigo-50 font-bold hover:bg-indigo-100 cursor-pointer' :
        // If package is being moved then show full storages as
        // grayed out
        isMovingPackage && storage.capacity <= storage.packageCount ?
          'text-gray-400' :
          'hover:bg-indigo-100 cursor-pointer'
    );
  };

  return (
    <Table className="w-full table-fixed">
      <Thead>
        <Tr>
          <Th className="w-16">ID</Th>
          <Th>Nimi</Th>
          <Th>Paketit</Th>
        </Tr>
      </Thead>
      {
        (storageListResponse.state === API_STATES.LOADING && !storageListResponse.response) &&
        <Tbody>
          <Tr>
            <Td colSpan={3}>
              <LoadingIndicator />
            </Td>
          </Tr>
        </Tbody>
      }
      {
        storageListResponse.state === API_STATES.ERROR &&
        <Tbody>
          <Tr>
            <Td colSpan={3}>
              <p>Virhe:</p>
              <pre>
                {JSON.stringify(storageListResponse.error, null, 2)}
              </pre>
            </Td>
          </Tr>
        </Tbody>
      }
      {
        (storageListResponse.state === API_STATES.SUCCESS || storageListResponse.response) &&
        <Tbody>
          {
            storageListResponse.response?.map(storage =>
              <Tr key={storage.id}
                  onClick={() => {
                    // When package is being moved, the row can be clicked only if
                    // there's free space left in the storage
                    if (!isMovingPackage || storage.capacity > storage.packageCount) {
                      selectStorage(storage.id);
                    }
                  }}
                  className={storageRowClassName(storage)}>
                <Td className="text-right">
                  #{storage.id}
                </Td>
                <Td>
                  {storage.name}
                </Td>
                <Td>
                  {storage.packageCount} / {storage.capacity}
                </Td>
              </Tr>
            )
          }
          <Tr>
            <Td colSpan={2}>
              <Input placeholder={"Lisättävän paketin nimi"}
                     value={newPackageName}
                     onKeyPress={addNewPackageOnEnter}
                     onChange={(e) => setNewPackageName(e.target.value)} />
            </Td>
            <Td>
              <Button disabled={isPackageAddingDisabled}
                      onClick={addNewPackageAndClearField}>Lisää</Button>
              {
                addNewPackageResponse.state === API_STATES.ERROR &&
                <div className="ml-3 inline-block">
                  Virhe: {JSON.stringify(addNewPackageResponse.error, null, 2)}
                </div>
              }
            </Td>
          </Tr>
        </Tbody>
      }
    </Table>
  );
};

// Checks whether there's free space left in any of the storages
function hasFreeSpaceLeft(storages: StorageListDto[] | null): boolean {
  if (storages) {
    for (const storage of storages) {
      if (storage.packageCount < storage.capacity) {
        return true;
      }
    }
  }
  return false;
}

export default StorageListView;
