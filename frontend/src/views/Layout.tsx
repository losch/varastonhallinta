import React from "react";
import { faBoxOpen, faMap, faList, faSignOutAlt } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {Link, useLocation } from "react-router-dom";

interface NavItemProps {
  icon: React.ReactElement;
  text: string;
  isActive?: boolean;
  to: string;
}

const NavItem = ({icon, text, isActive, to}: NavItemProps) =>
  <Link
    to={to}
    className={
      "block pl-2 md:pl-4 py-2 duration-150 ease-out transform hover:bg-gray-300 hover:text-gray-800 cursor-pointer transition-all" +
      (isActive ? ' bg-gray-500 text-gray-100' : ' bg-gray-700 text-gray-300')}>
    <span className="text-sm">{icon}</span>
    <span className="pl-2 hidden md:inline-block font-medium">{text}</span>
  </Link>;

export interface LayoutProps {
  children?: React.ReactNode;
}

const Layout = ({children}: LayoutProps) => {
  let location = useLocation();

  return (
    <div className="h-screen w-screen bg-white bg-gray-400">
      <div className="flex">
        <div className="h-screen w-8 md:w-56 bg-gray-700 text-white font-open-sans shadow-xl">
          <h1 className="hidden md:block font-bold text-xl my-3 text-gray-150 font-openSans select-none text-center">
            <FontAwesomeIcon icon={faBoxOpen} /><span className="pl-2">Varastonhallinta</span>
          </h1>
          <h1 className="md:hidden font-bold text-xl my-3 text-gray-150 text-center"><FontAwesomeIcon icon={faBoxOpen} /></h1>

          <NavItem
            icon={<FontAwesomeIcon icon={faList} />}
            to="/storages"
            text="Hyllyt"
            isActive={location.pathname.startsWith('/storages') ||
                      location.pathname === '/'} />

          <NavItem
            icon={<FontAwesomeIcon icon={faMap} />}
            to="/map"
            text="Kartta"
            isActive={location.pathname === '/map'} />

          <NavItem
            icon={<FontAwesomeIcon icon={faSignOutAlt} />}
            to="/logout"
            text="Kirjaudu ulos"
            isActive={location.pathname === '/logout'} />
        </div>

        <div className="flex-1 p-3 max-h-screen overflow-y-auto">
          {children}
        </div>
      </div>

    </div>
  );
};

export default Layout;
