import {ApiRequest} from "../hooks/apiRequest";

/**
 * Fetch storage list
 */
export function fetchStorageList(): ApiRequest<StorageListDto[]> {
  return {
    url: '/api/storages/list',
    method: 'get',
    headers: {
      'Content-Type': 'application/json'
    }
  }
}

export interface StorageListDto {
  id: number;
  name: string;
  capacity: number;
  packageCount: number;
}


/**
 * Fetch single storage
 * @param id Storage ID
 */
export function fetchStorage(id: number): ApiRequest<StorageDto> {
  return {
    url: `/api/storages/${id}`,
    method: 'get',
    headers: {
      'Content-Type': 'application/json'
    }
  }
}

export interface PackageDto {
  id: number;
  name: string;
}

export interface StorageDto {
  id: number;
  name: string;
  capacity: number;
  packages: PackageDto[];
}


/**
 * Fetch storage's name
 * @param id Storage ID
 */
export function fetchStorageName(id: number): ApiRequest<string> {
  return {
    url: `/api/storages/${id}/name`,
    method: 'get',
    headers: {
      'Content-Type': 'text/plain'
    }
  }
}


/**
 * Adds new package to first available storage
 * @param name Package's name to add
 * @return Storage's ID where the package was added
 */
export function addPackage(name: string): ApiRequest<number> {
  return {
    url: `/api/storages/addPackage`,
    body: name,
    method: 'post',
    headers: {
      'Content-Type': 'text/plain'
    }
  }
}

/**
 * Moves package to another storage
 * @param id Package ID
 * @param toStorageId Storage ID
 */
export function movePackage(id: number, toStorageId: number): ApiRequest<any> {
  return {
    url: `/api/storages/${id}/move?toStorageId=${toStorageId}`,
    method: 'post',
    headers: {
      'Content-Type': 'application/json'
    }
  }
}

/**
 * Deletes package
 * @param id Package ID
 */
export function deletePackage(id: number): ApiRequest<any> {
  return {
    url: `/api/storages/deletePackage/${id}`,
    method: 'delete',
    headers: {
      'Content-Type': 'text/plain'
    }
  }
}
