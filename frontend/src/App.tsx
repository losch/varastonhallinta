import React, {useEffect} from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Layout from "./views/Layout";
import StorageRoutes from './views/Storages';
import StorageMapRoutes from "./views/StorageMap";
import Panel, {PanelBody} from "./components/Panel";

const Logout = () => {
  useEffect(() => {
    // Set authentication cookie to expire and redirect to login page
    document.cookie = 'varastonhallinta-credential=; Max-Age=0';
    window.location.href = '/login.html?loggedOut=true';
  });
  return (
    <Panel>
      <PanelBody>
        Kirjaudutaan ulos...
      </PanelBody>
    </Panel>
  );
};

function App() {
  return (
    <Router>
      <Layout>
        <Switch>
          <Route exact path="/map">
            <StorageMapRoutes />
          </Route>

          <Route path="/storages">
            <StorageRoutes />
          </Route>

          <Route path="/logout">
            <Logout />
          </Route>

          <Route path="/">
            <StorageRoutes />
          </Route>
        </Switch>
      </Layout>
    </Router>
  );
}

export default App;
