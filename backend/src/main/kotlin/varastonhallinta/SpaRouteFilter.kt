package varastonhallinta

import java.io.IOException
import java.util.regex.Pattern
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.annotation.WebFilter
import javax.servlet.http.HttpFilter
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Filter for serving index.html for every route that is handled
 * by the frontend.
 *
 * https://quarkus.io/blog/quarkus-and-web-ui-development-mode/
 */
@WebFilter(urlPatterns = ["/*"])
class SpaRouteFilter : HttpFilter() {

    @Throws(IOException::class, ServletException::class)
    override fun doFilter(req: ServletRequest, res: ServletResponse, chain: FilterChain) {
        val request: HttpServletRequest = req as HttpServletRequest
        val response: HttpServletResponse = res as HttpServletResponse

        chain.doFilter(request, response)

        if (response.status == 404) {
            val path: String = request.requestURI
                                      .substring(request.contextPath.length)
                                      .replace("[/]+$", "")

            if (!FILE_NAME_PATTERN.matcher(path).matches()) {
                // We could not find the resource, i.e. it is not anything known to the server (i.e. it is not a REST
                // endpoint or a servlet), and does not look like a file so try handling it in the front-end routes
                // and reset the response status code to 200.
                response.status = 200
                request.getRequestDispatcher("/")
                       .forward(request, response)
            }
        }
    }

    companion object {
        private val FILE_NAME_PATTERN: Pattern = Pattern.compile(".*[.][a-zA-Z\\d]+")
    }
}
