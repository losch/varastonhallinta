package varastonhallinta.entity

import javax.persistence.*

@Entity
open class Package {

    @Id
    @SequenceGenerator(name="package_id_generator", sequenceName = "package_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "package_id_generator")
    open var id: Long? = null

    open lateinit var name: String

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "storage_id")
    open lateinit var storage: Storage
}
