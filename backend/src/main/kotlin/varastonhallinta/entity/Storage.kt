package varastonhallinta.entity

import javax.persistence.*

@Entity
open class Storage {

    @Id
    @SequenceGenerator(name="storage_id_generator", sequenceName = "storage_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "storage_id_generator")
    open var id: Long? = null

    open lateinit var name: String

    open var capacity: Int = 0

    @Column("package_count")
    open var packageCount: Int = 0

    @OneToMany(mappedBy = "storage")
    @OrderBy("id")
    open var packages: MutableList<Package> = mutableListOf()
}
