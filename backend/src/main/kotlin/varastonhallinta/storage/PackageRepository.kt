package varastonhallinta.storage

import io.quarkus.hibernate.orm.panache.kotlin.PanacheRepository
import varastonhallinta.entity.Package
import javax.enterprise.context.ApplicationScoped
import javax.transaction.Transactional

@ApplicationScoped
@Transactional(Transactional.TxType.MANDATORY)
class PackageRepository : PanacheRepository<Package> {
}
