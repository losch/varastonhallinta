package varastonhallinta.storage

import varastonhallinta.entity.Package
import varastonhallinta.storage.dto.PackageDto
import varastonhallinta.storage.dto.StorageDto
import varastonhallinta.storage.dto.StorageListDto
import java.lang.RuntimeException
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject
import javax.persistence.LockModeType
import javax.transaction.Transactional

@ApplicationScoped
@Transactional
class StorageService {

    @Inject
    internal lateinit var storageRepository: StorageRepository

    @Inject
    internal lateinit var packageRepository: PackageRepository

    /**
     * Lists all storages
     */
    fun listStorages(): List<StorageListDto> = storageRepository.listStorages()

    /**
     * Get storage by ID
     */
    fun getStorage(id: Long): StorageDto {
        val storage = storageRepository.findById(id)!!
        return StorageDto(
            id = storage.id!!,
            name = storage.name,
            capacity = storage.capacity,
            packages = storage.packages.map {
                PackageDto(
                    id = it.id!!,
                    name = it.name
                )
            }
        )
    }

    /**
     * Gets storage name by storage ID
     */
    fun getStorageName(id: Long): String = storageRepository.findNameById(id)

    /**
     * Adds package to a first available storage
     */
    fun addPackage(name: String): Long {
        val storage = storageRepository.findFreeStorageWithLock()

        if (storage == null) {
            throw RuntimeException("No free storages left")
        }
        else {
            val newPackage = Package().apply {
                this.name = name
                this.storage = storage
            }
            packageRepository.persistAndFlush(newPackage)
            return storage.id!!
        }
    }

    /**
     * Moves package to another storage
     */
    fun movePackage(id: Long, toStorageId: Long) {
        // Find the target storage and lock the row for the rest of the
        // transaction so the same storage cannot be accessed by someone
        // else at the same time.
        val storage = storageRepository.findById(toStorageId, LockModeType.PESSIMISTIC_WRITE)

        if (storage == null) {
            throw RuntimeException("Storage $toStorageId not found")
        }
        else if (storage.packageCount >= storage.capacity) {
            throw RuntimeException("Storage $toStorageId does not have free space")
        }
        else {
            val pkg = packageRepository.findById(id)!!
            pkg.storage = storage
            packageRepository.persistAndFlush(pkg)
        }
    }

    /**
     * Deletes package
     */
    fun deletePackage(packageId: Long) {
        packageRepository.deleteById(packageId)
    }
}
