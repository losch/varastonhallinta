package varastonhallinta.storage.dto

import io.quarkus.runtime.annotations.RegisterForReflection

@RegisterForReflection
data class StorageDto(
    val id: Long,
    val name: String,
    val capacity: Int,
    val packages: List<PackageDto>
)
