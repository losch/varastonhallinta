package varastonhallinta.storage.dto

import io.quarkus.runtime.annotations.RegisterForReflection

@RegisterForReflection
data class StorageListDto(
    val id: Long,

    // Storage's name
    val name: String,

    // Storage's maximum capacity
    val capacity: Int,

    // Amount of packages in the storage
    val packageCount: Int
)
