package varastonhallinta.storage.dto

import io.quarkus.runtime.annotations.RegisterForReflection

@RegisterForReflection
data class PackageDto(
    val id: Long,
    val name: String
)
