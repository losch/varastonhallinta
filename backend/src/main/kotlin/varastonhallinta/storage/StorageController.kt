package varastonhallinta.storage

import varastonhallinta.storage.dto.StorageDto
import varastonhallinta.storage.dto.StorageListDto
import javax.inject.Inject
import javax.ws.rs.*
import javax.ws.rs.core.MediaType

@Path("/api/storages")
class StorageController {

    @Inject
    internal lateinit var storageService: StorageService

    @GET
    @Path("list")
    @Produces(MediaType.APPLICATION_JSON)
    fun list(): List<StorageListDto> = storageService.listStorages()

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    fun getById(@PathParam("id") id: Long): StorageDto = storageService.getStorage(id)

    @GET
    @Path("{id}/name")
    @Produces(MediaType.TEXT_PLAIN)
    fun getStorageName(@PathParam("id") id: Long): String = storageService.getStorageName(id)

    @POST
    @Path("addPackage")
    @Produces(MediaType.TEXT_PLAIN)
    fun addPackage(name: String): Long = storageService.addPackage(name)

    @POST
    @Path("{id}/move")
    @Produces(MediaType.TEXT_PLAIN)
    fun movePackage(@PathParam("id") id: Long,
                    @QueryParam("toStorageId") toStorageId: Long) = storageService.movePackage(id, toStorageId)

    @DELETE
    @Path("deletePackage/{packageId}")
    @Produces(MediaType.TEXT_PLAIN)
    fun deletePackage(@PathParam("packageId") packageId: Long) = storageService.deletePackage(packageId)
}
