package varastonhallinta.storage

import io.quarkus.hibernate.orm.panache.kotlin.PanacheRepository
import io.quarkus.panache.common.Parameters
import io.quarkus.panache.common.Sort
import varastonhallinta.entity.Storage
import varastonhallinta.storage.dto.StorageListDto
import javax.enterprise.context.ApplicationScoped
import javax.persistence.LockModeType
import javax.transaction.Transactional

@ApplicationScoped
@Transactional(Transactional.TxType.MANDATORY)
class StorageRepository : PanacheRepository<Storage> {

    /**
     * Lists storages
     */
    fun listStorages(): List<StorageListDto> {
        return findAll(Sort.by("id"))
            .project(StorageListDto::class.java)
            .list()
    }

    /**
     * Returns storage name by ID
     */
    fun findNameById(id: Long): String {
        @Suppress("CAST_NEVER_SUCCEEDS")
        return find(
                """
                select new java.lang.String(s.name)
                from Storage s
                where :id = id
                """.trimIndent(),
                Parameters.with("id", id)
            )
            .singleResult() as String
    }

    /**
     * Returns a storage which has free space. Locks the storage for the
     * rest of the transaction.
     */
    fun findFreeStorageWithLock(): Storage? =
        find("packageCount < capacity", Sort.by("id"))
            .withLock(LockModeType.PESSIMISTIC_WRITE)
            .firstResult()
}
