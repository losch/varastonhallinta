package varastonhallinta.storage

import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.security.TestSecurity
import io.restassured.RestAssured
import org.hamcrest.CoreMatchers
import org.junit.jupiter.api.Test

@QuarkusTest
class StorageControllerTest {

    @Test
    @TestSecurity(user = "testUser", roles = ["user"])
    fun `getById returns storage`() {
        RestAssured.given()
            .`when`().get("/api/storages/1")
            .then()
            .statusCode(200)
            .body(
                CoreMatchers.`is`(
                    "{\"id\":1," +
                    "\"name\":\"Hylly 1\"," +
                    "\"capacity\":4," +
                    "\"packages\":[" +
                      "{\"id\":1,\"name\":\"Laatikko 1\"}," +
                      "{\"id\":2,\"name\":\"Paketti 1\"}," +
                      "{\"id\":3,\"name\":\"Laatikko 2\"}," +
                      "{\"id\":4,\"name\":\"Paketti 2\"}]}"
                )
            )
    }

    @Test
    @TestSecurity(user = "testUser", roles = ["user"])
    fun `getStorageName returns storage's name`() {
        RestAssured.given()
          .`when`().get("/api/storages/1/name")
          .then()
             .statusCode(200)
             .body(CoreMatchers.`is`("Hylly 1"))
    }
}
