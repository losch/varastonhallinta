package varastonhallinta.storage

import io.quarkus.test.TestTransaction
import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.lang.RuntimeException
import javax.inject.Inject

@QuarkusTest
class StorageServiceTest {

    @Inject lateinit var storageService: StorageService

    @Test
    @TestTransaction
    fun `addPackage adds a package to first available storage`() {
        val storageId = storageService.addPackage("package from unit test")
        assert(storageId == 2L)
    }

    @Test
    @TestTransaction
    fun `addPackage fails if there's no free space left in storages`() {
        assertThrows<RuntimeException> {
            // 100 is a large number enough so that all storages get filled
            for (i in 0..100) {
                storageService.addPackage("Package $i")
            }
        }
    }

    @Test
    @TestTransaction
    fun `movePackage moves a package from a storage to another storage`() {
        storageService.movePackage(1L, 2L)

        // Package 1 should be in storage 2
        val storage = storageService.getStorage(2L)
        assert(storage.packages.find { it.id == 1L } != null)
    }

    @Test
    @TestTransaction
    fun `movePackage fails if target storage is full`() {
        assertThrows<RuntimeException> {
            storageService.movePackage(6L, 1L)
        }
    }
}
