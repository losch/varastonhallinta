#
# Common commands for running the application
#

# Starts up the database
run-database:
	docker-compose up

# Starts up backend in development mode
run-backend:
	(cd backend && ./mvnw compile quarkus:dev)

# Cleans everything
clean:
	./mvnw clean

# Builds the application as über jar
build:
	./mvnw package -Dquarkus.package.type=uber-jar

# Builds the application as a native app
build-native:
	./mvnw package -Pnative -Dquarkus.native.container-build=true

# Runs the built .jar file
run-jar:
	java -jar backend/target/backend-1-runner.jar

.PHONY: run-database run-backend clean build build-native run-jar
